#!/usr/bin/python3
import json
import pathlib
import re
import subprocess
import sys
import tomllib
from types import SimpleNamespace as Ns

import ldap


base_cmd = [ 'php', '/var/www/owncloud/occ' ]
def get_ldap_users(cfg):
	base = ldap.initialize(cfg.server)
	base.simple_bind_s(cfg.binddn, cfg.passwd)
	users = base.search_s(cfg.basedn, ldap.SCOPE_ONELEVEL,
		'objectClass=inetOrgPerson', ['uid']
	)
	base.unbind()
	return { u['uid'][0].decode() for _,u in users }


def get_owncloud_users():
	cmd = base_cmd + [ '--output=json', 'files_external:list' ]
	reg = re.compile(r'datadir: "\\/home-owncloud\\/(?P<uid>[a-z][a-z0-9-]+)"')
	with subprocess.Popen(cmd, stdout=subprocess.PIPE, text=True) as p:
		users = map(
			lambda d: reg.fullmatch(d['configuration']), json.load(p.stdout)
		)
	return { u.group('uid') for u in users if mount is not None }


def create_mounts(users):
	cmd = base_cmd + [ 'files_external:import', '-' ]
	mounts = [
		{
			'mount_point': '\/Crans',
			'storage': '\OC\Files\Storage\Local',
			'authentication_type': r'null::null',
			'configuration': {
				'datadir': r'/home-owncloud/' + uid,
			},
			'options': '',
			'applicable_users': [ uid ],
			'applicable_group': [],
		} for uid in users
	]
	with subprocess.Popen(cmd, stdin=subprocess.PIPE, text=True) as p:
		json.dump(mounts, p.stdin)


path = pathlib.Path(__file__).absolute().parent
if __name__ == '__main__':
	with open(path / 'owncloud.toml', 'rb') as file:
		cfg = Ns(**{ k: Ns(**v) for k,v: tomllib.load(file).items() })
	ldap_users = get_ldap_users(cfg.ldap)
	owncloud_users = get_owncloud_users()
	create_mounts(ldap_users - owncloud_user)
